package com.example.ronal.sextapracticajavier;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button botonS, botonP;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    botonS = (Button) findViewById(R.id.botonS);
    botonP = (Button) findViewById(R.id.botonP);

    botonS.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, SensorAcelerometro.class);
            startActivity(intent);
        }
    });
    botonP.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, SensorProximity.class);
            startActivity(intent);
        }
    });
}
    }

